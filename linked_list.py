#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 15:42:47 2021

@author: angelina
@origin : Olivier Chabrol
"""

class Node:
    """ Node of a list """
    # pylint: disable=too-few-public-methods

    def __init__(self, data):
        """
        Method to initialize the node object

        The value of the data will be defined by the value passed through the constructor,
        and the pointer(next) will be initially set to null.
        """
        self.data = data
        self.next = None

    def __repr__(self):
        """
        Displays the data
        """
        return self.data

class LinkedList:
    """ Linked list """
    def __init__(self, nodes=None):
        """
        Method to initialize the Linked list.

        We initialize each element of the list entered by the user as a node
        and we add a link to it, to obtain a chained list.

        Parameters
        ----------
        nodes : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        None.

        """
        self.head = None
        if nodes is not None and len(nodes) != 0:
            node = Node(data=nodes.pop(0)) # Retrieves the first node of the chained list.
            self.head = node

        # Browse the list and create a node and link for each item.
            for elem in nodes:
                node.next = Node(data=elem)
                node = node.next


    def first_node(self):
        """
        Returns the first node.
        """
        return self.head


    def empty(self):
        """
        Returns teh empty list
        """
        return not self.head


    def get(self, index):
        """
        Method that retrieves the node that is at the position entered by the user (index).

        Parameters
        ----------
        index : int
            Position of the element to be recovered.

        Raises
        ------
        Exception
            Returns an exception if the list is empty.

        Returns
        -------
        None.

        """
        if self.head is None:
            raise Exception("The list is empty")

        self.recurs(index, self.head)

    def recurs(self, index, node):
        """
        Recursion method.

        Parameters
        ----------
        index : int
            Position of the element to be recovered.
        node : str
            From which node you want to start recursion.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        print(index, node)
        if node is None:
            return node

        if index == 0:
            return node

        return self.recurs(index - 1, node.next)


    def add_after(self, data, new_node):
        """
        Method that allows to insert a new node after the node with the value == node

        Parameters
        ----------
        data : str
            Searched data, the node after which you want to insert the new node.
        new_node : str
            Node to insert, contains the value for the new node..

        Raises
        ------
        Exception
            Returns an exception if the list is empty,
            and/or if the data item is not found

        Returns
        -------
        None.

        """
        # Check if the list is empty
        if not self.head:
            raise Exception("List is empty")

        for node in self:
            if node.data == data:

            # The pointer(next) of the new_node is set to link stored by item
            # and the link of item is set to new_node.
                new_node.next = node.next
                node.next = new_node
                return

        raise Exception("Node with data '{}' not found".format(data))


    def add_before(self, data, new_node):
        """
        Method that allows to insert a new node before the node with the value == node

        Parameters
        ----------
        data : str
            Searched data, the node before which you want to insert the new node.
        new_node : str
            Node to insert, contains the value for the new node.

        Raises
        ------
        Exception
            Returns an exception if the list is empty,
            and/or if the data item is not found

        Returns
        -------
        None.

        """
        if not self.head:
            raise Exception("List is empty")

        if self.head.data == data:
            return self.add_first(new_node)


        prev_node = self.head

        for node in self:
            if node.data == data:
                prev_node.next = new_node
                new_node.next = node
                return data, new_node
            prev_node = node

        raise Exception("Node with data '{}' not found".format(data))


    def remove_node(self, data):
        """
        Method that allows to delete all node(s) value == node.

        Parameters
        ----------
        data : str
            Searched data to delete.

        Raises
        ------
        Exception
            Returns an exception if the list is empty,
            and/or if the data item is not found

        Returns
        -------
        None.

        """
        if not self.head:
            raise Exception("List is empty")

        # We check if the item to be deleted is at the beginning of the chained list.
        # If True , it is deleted by defining the second node as the first node.
        if self.head.data == data:
            self.head = self.head.next
            return

        previous_node = self.head
        for node in self:
            if node.data == data:
                previous_node.next = node.next
                return
            previous_node = node

        raise Exception("Node with data '{}' not found".format(data))


    def add_first(self, node_to_add):
        """
        Method that inserts a node at the first element of the list.

        Parameters
        ----------
        node_to_add : TYPE
            Node to insert, contains the value for the new node.

        Returns
        -------
        None.

        """
        node_to_add.next = self.head
        self.head = node_to_add


    def add_last(self, node_to_add):
        """
        Method that inserts a node at the last element of the list.

        Parameters
        ----------
        node_to_add : str
            Node to insert, contains the value for the new node.

        Returns
        -------
        None.

        """
        if self.head is None:
            self.head = node_to_add
            return

        node = self.head
        # while node.next is not None:*
        while node.next is not None:
            node = node.next
        node.next = node_to_add


    def __repr__(self):
        """
        Method to display the chained list.

        Returns
        -------
        str
            Displays the chained list.

        """
        node = self.head
        nodes = []
        while node is not None:
            nodes.append(node.data) # added items to the list.
            node = node.next # each node has a link
        #return "a"
        return "{}".format(nodes)


    def __iter__(self):
        """
        Iteration method
        """
        node = self.head
        while node is not None:
            yield node
            node = node.next
