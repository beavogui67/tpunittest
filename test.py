#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 15:42:47 2021

@author: angelina
"""

from __future__ import absolute_import
import unittest
from linked_list import LinkedList, Node


class Test(unittest.TestCase):
    """
    Class used to test the functions of the LinkedList class.
    """
    def setUp(self):
        """
        Create an empty list and a list containing nodes.
        """
        self.empty_list = LinkedList([])
        self.list = LinkedList(['a', 'b', 'c', 'd', 'e'])


    def test_empty_list(self):
        """
        Checks that the chained list is empty.
        """
        self.assertTrue(self.empty_list.empty())
        # assertTrue, allows to say that the given assertion is true,
        # so the list is empty.


    def test_not_empty(self):
        """
        Checks that any chained list on which an item has just been stacked is not empty.
        """
        self.list.add_after('b', Node('Z'))
        self.assertFalse(self.list.empty())
        # assertFalse, allows you to say that the given assertion is false,
        # so that the list is not empty.


    def test_unchanged(self):
        """
        Checks that a chained list that has undergone stacking followed
        by unstacking is unchanged.
        """
        self.list.add_before('e', Node('T'))
        self.list.remove_node('T')
        self.assertEqual(self.list.__repr__(), "['a', 'b', 'c', 'd', 'e']")
        # Retrieves the modified list, uses the __repr__ method to display the chained list
        # and compares it to the initial chained list.
        # assertEqual, checks whether two elements are equal.


    def test_first_node(self):
        """
        Checks the top of the chained list on which an element e has just been stacked is e.
        """
        self.list.add_first(Node(3))
        self.assertEqual(self.list.first_node().__repr__() , 3)
        # Retrieves the first list element (first_node method)
        # and uses the __repr__ method to display the node.
        # assertEqual, checks whether two elements are equal.


if __name__ == '__main__':
    unittest.main()
