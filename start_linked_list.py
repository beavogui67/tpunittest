#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 15:42:47 2021

@author: angelina
@origin : Olivier Chabrol
"""
from __future__ import absolute_import
from linked_list import LinkedList
from linked_list import Node

if __name__ == "__main__":
    llist = LinkedList(["a", "b", "c", "d", "e"])
    llist.get(3)
    print(llist.recurs(3,llist.head))
    llist.add_last(Node("g"))
    print(llist)
    llist.add_first(Node("z"))
    llist.add_before("z", Node("y"))
    llist.add_before("y", Node("x"))
    llist.add_before("g", Node("f"))
    llist.add_after("g", Node("h"))
    llist.remove_node("a")
    print(llist)
    llist.remove_node("x")
    print(llist)
